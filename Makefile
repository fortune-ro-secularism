SRC=laicitate ateism antiteism

default: laicitate.dat ateism.dat antiteism.dat

%.dat: %
	strfile $(basename $<)

clean:
	rm -f *.dat
	rm -f *~

.PHONY: default clean
